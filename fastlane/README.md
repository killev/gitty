fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios bootstrap
```
fastlane ios bootstrap
```
Setup all pre-requisites
### ios tests
```
fastlane ios tests
```
Runs tests and checks coverage
### ios alpha
```
fastlane ios alpha
```
Runs build and upload  archive to Crashlytics, supposed to be used on feaure/bug branches
### ios bump
```
fastlane ios bump
```

### ios beta
```
fastlane ios beta
```
Runs build and upload  archive to Testfight, supposed to be used on master branch
### ios play
```
fastlane ios play
```
Playground

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).

require 'rest-client'
require 'base64'

#update_fastlane


def is_ci
  return ENV['CI'] ? true : false
end


def ci_commit
  return ENV['CI_COMMIT_MESSAGE'] ? ENV['CI_COMMIT_MESSAGE'] : last_git_commit[:message]
end


def ci_branch
  return ENV['CI_COMMIT_REF_NAME'] ? ENV['CI_COMMIT_REF_NAME'] : git_branch
end

def to_percent(n)
  (n.to_f * 100.0).round(2).to_s
end

def tags_gitlab(branch:, project_id:, token:) 
  response = RestClient.get(
    "https://gitlab.com/api/v4/projects/#{project_id}/repository/tags?per_page=100&ref_name=#{branch}",
    { "PRIVATE-TOKEN" => token, content_type: :json}
  )
  tags = JSON.parse(response)
    .map{|x| {:name => x["name"], :created_at => x["commit"]["created_at"] } } 
  return tags
end

def branches_gitlab(branch:, project_id:, token:) 
  response = RestClient.get(
    "https://gitlab.com/api/v4/projects/#{project_id}/repository/branches?per_page=100&ref_name=#{branch}",
    { "PRIVATE-TOKEN" => token, content_type: :json}
  )
  branches = JSON.parse(response)
    #.map{|x| {:name => x["name"], :created_at => x["commit"]["created_at"] } } 
  return branches
end


def file_to_gitlab_action(git_file:)
  split = git_file.split
  file_path = split[1]
  content = File.read("../"+file_path)
  decode_base64_content = Base64.encode64(content)

  {
    "action" => "update",
    "file_path"=> file_path,
    "content"=> decode_base64_content,
    "encoding"=> "base64",
  }
end

def change_log_gitlab(branch:, project_id:, token:, since:)
	
  response = RestClient.get(
    "https://gitlab.com/api/v4/projects/#{project_id}/repository/commits?per_page=100&ref_name=#{branch}&since=#{since}",
    { "PRIVATE-TOKEN" => token, content_type: :json}
  )
  
  issues = JSON.parse(response)
    .map{|x| x["message"]}
    .map{|x| x.match(/Closes #(\d+)/)}
    .map{|x| x[1] if x }.select{|x| x}.select{|x| x!="0"}

  titles = issues.map do |item|
    response = RestClient.get(
    	"https://gitlab.com/api/v4/projects/#{project_id}/issues/#{item}",
      { "PRIVATE-TOKEN" => token, content_type: :json})
     
    json = JSON.parse(response)
    { "web_url" => json["web_url"], "title" => json["title"] }
  end
  
  dones = titles.map {|x| "* #{x["title"]} - #{x["web_url"]}"}.join("\r\n")
  return "Completed tasks:\r\n\r\n#{dones}\r\n\r\n"
end

def change_alpha_log_gitlab(branch:, project_id:, token:)
	
  response = RestClient.get(
    "https://gitlab.com/api/v4/projects/#{project_id}/repository/commits?per_page=100&ref_name=#{branch}",
    { "PRIVATE-TOKEN" => token, content_type: :json}
  )

  messages = JSON.parse(response)

  first_commit_idx = messages
    .index{|x| /^Bump Version/.match(x["title"])}

  UI.message "first_commit_idx"
  UI.message first_commit_idx

  return messages.first(first_commit_idx)
    .map{|x| x["message"]}
    .map{|x| x.lines.first }
    .join("\r\n")

  return messages
  
  #return "Completed tasks:\r\n\r\n#{dones}\r\n\r\n"
end


def create_release_tag(sha:, project_id:, token:, message:, version:, description:)

  payload = {
   "tag_name" => "#{version}",
   "commit_message" => message,
   "ref" => sha,
   "release_description" => description
  }
	UI.message payload
  response = RestClient.post(
    "https://gitlab.com/api/v4/projects/#{project_id}/repository/tags",
    payload,
    { "PRIVATE-TOKEN" => token, content_type: :json}
  )
  response
end


def commit_to_gitlab(branch:, message:, project_id:, token:)
  
  repo_status = Actions.sh("git status --porcelain").split("
")
  UI.message "Commit files: #{ repo_status }"
  actions = repo_status.map! {|x| file_to_gitlab_action(git_file: x)}
  UI.message "To branch: #{branch}"
 
  payload = {
    "branch" => branch,
    "commit_message" => message,
    "actions" => actions
  }
  
  response = RestClient.post(
    "https://gitlab.com/api/v4/projects/#{ project_id }/repository/commits",
    payload,
    { "PRIVATE-TOKEN" => token, content_type: :json}
  )
  UI.message "response: #{ JSON.parse(response) }"
  if !is_ci
    Actions.sh("git reset --hard")
    Actions.sh("git pull")
  end 
end

def next_build(bundle:, release:, next1:)
  UI.message "Getting version form service: #{ bundle }, release #{ release }, update version #{ not next1 }"

  action = next1 ? "next" : "release"
  url = "https://yb8h3houo9.execute-api.us-east-1.amazonaws.com/prod/version/#{ bundle }/#{ action }/#{ release }"
  
  response = RestClient.get(url, {accept: :json})
  version = JSON.parse(response)["version"]
  version_number = "#{ version['major'] }.#{ version['minor'] }.#{ version['hotfix'] }"
  build_number = version["build"]
    
  res = { "version_number" => version_number, "build_number" => build_number }
  UI.message "Get new version: #{ res }"
  return res
end

def increment_build(bundle:, release:)
  
  new_build = next_build(bundle: bundle, release: release, next1: true)
  UI.message "increment_build_number: #{ new_build["build_number"] }"
  UI.message "increment_version_number: #{ new_build["version_number"] }"


  increment_build_number(build_number: new_build["build_number"])
  increment_version_number(version_number: new_build["version_number"])
  
  result = "#{ new_build["version_number"] }(#{ new_build["build_number"] })"
  result
end

def release_build(bundle:, release:)
  new_build = next_build(bundle: bundle, release: release, next1: false)
  return "#{ new_build["version_number"] }"
end